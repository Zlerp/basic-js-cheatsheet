# Basic Javascript Cheat Sheet

---

# Table of Contents
- [Escapes](#escapes)
- [Booleans](#boolean)
- [Arrays](#array)
- [Index](#indexes)
- [Methods](#methods)
- [Variables](#variable)
- [Incrementing](#addition)
- [Script Tags](#scripts)
- *Loops*
  * [While Loops](#while)
  * [For-Loops](#for)
  * [Example](#loop-example)
- *Conditionals*
	- [If](#conditionals)
	- [Case/Switches](#switch)



---



## <a name="escapes"></a>Special escape characters for strings

`\t` => escape character to add tab into string  
`\"` => escape character to add a quotation in string  
`\\` => escape character to add a backslash in string  
`\n` => escape character to create a new line

---  

## <a name="boolean"></a>Boolean -> True or False

*Compares values with and expression and outputs a `true` or `false`*

`4 > 6` => `false`  
`4 < 6` => `true`  
`4 == 7` => `false`  
`10 != 10` => `false`  
`10 != 12` => `true`  


#### And's

_`&&` is used to compare to conditions, if **BOTH** conditions are met, then it is true_  

`1 < 6 && 2 < 3` => `true`
`1 < 12 && -5 > 3` => `false`
`1 > 50 && -20 > 20` => `false`

#### Or's

_`||` is used to compare to conditions, if **EITHER** conditions are met, then it is true_  

`1 < 6 || 2 < 3` => `true`
`1 > 12 || -5 < 3` => `true`
`1 < 50 || -20 < 20` => `true`
---

## <a name="array"></a>Arrays

*Arrays are used to store multiple values*

```javascript
  var num = [1, 2, 3, 20, 17, 35]
  numbers.length
```
=> `6`  

*there are 6 numbers in the variable `num`*


---

## <a name="indexes"></a>Indexes

*Indexes are the position of the character of the string or a value of an array.*

*INDEXES ALWAYS START AT ZERO!!!!*  

`"hello world"` at index  `0` is `"h"`  
`"hello world"` at index  `1` is `"e"`  
`"hello world"` at index  `5` is `" "`  
`"hello world"` at index  `9` is `"l"`  

```javascript
  var num = [1, 2, 3, 20, 17, 35]
  numbers[3]
```
=> `20`  

*this asks for the value at index `3` of the variable `num`*

---



## <a name="methods"></a>Methods


#### console.log()

*prints string or variables to the console.*

```javascript
  console.log("Hello World!");
```
=> `"Hello World!"`


#### .length
```javascript
"to find the length of this string".length
```  
=> `33`  
*gives you length of string, 33 in this case*

#### .charAt()
```javascript
  "hello".charAt(1)
```
=> `e`   


####  alert()

```javascript
  alert("Hello World!");
```
=>  outputs alert popup: `"Hello World!"`


*gives you the character of the string at the index of 1*


---


## <a name="variable"></a>Variables

*Variables are used to store data including strings and integers and more. You can use methods on variables and call them later in your script.*

```javascript
  var trainWhistles = 3
  trainWhistles
```
=> `3`  
=>  this variable can be called upon later.



## <a name="addition"></a>Types of Incrementing and Decrementing

`++` => plus one  
`--` => minus one  
`+=3` => increment by 3  
`-=50` => decrement by 50  

```javascript
var trainWhistles = 3
trainWhistles ++
```
=> `4`



```javascript
  var trainWhistles = 3
  trainWhistles --
```
=> `2`

---


## <a name="scripts"></a>Adding Scripting Tags to HTML

the script tag is added in the `<head><script src="your-JS-File-Path.js"></script></head>` your html files.  


```html
<html>
<head>
  <script src="js/script.js" charset="utf-8"></script>  
</head>
<body>
  <h1>Javascript Shit</h1>
</body>
</html>
```

*the files is `script.js` located in the folder called `js` in the root of the application*  
*be sure to navigate to the correct file location!!!!*  
*to access files that are in a previous folder, use a `../` like so:*  

```javascript
<script src="../js/script.js" charset="utf-8"></script>  

```

*this would access a folder one up from the location of the `index.html` file, and inside the folder called `js`*


---

## <a name="while"></a>While Loops

*runs code as long as Boolean expression is true*  
*iterates through arrays and more to do more with less code.*

Loop Syntax:

```javascript
  while(/*some expression is true*/){
    // 'do this code'
  }
```


### Basic While Loops:

```javascript
  var number = 1;
  while (number <= 5) {
    console.log(number);
    number++;
  }
```
*logs variable `number` and adds one to the variable each iteration until the number reaches `5`.*



### Multiple Variable While Loop Example  

```javascript
var numSheep = 4;
var monthNumber = 1;
var monthsToPrint = 4;

while (monthNumber <= monthsToPrint){
  numSheep = numSheep * 4;
  console.log("There will be " + numSheep + " sheep after " + monthNumber + " month(s)!");
  monthNumber++;
};
```
=> prints to console:

    There will be 16 sheep after 1 month(s)!  
    There will be 64 sheep after 2 month(s)!  
    There will be 256 sheep after 3 month(s)!  
    There will be 1024 sheep after 4 month(s)!  

---


## <a name="for"></a>For Loops

*like a while loop, for loops iterate through code until the statement is false*
Loop Syntax:


```javascript
  for ( /*start with this*/ ; /*loop if this expression true*/ ; /*do this after each loop*/) {
    // Loop This
  }
```


### Basic For loops

```javascript
for (var num = 4; num >= 1; num--) {
  console.log(num);
};
```
=> logs to console:  

    4  
    3  
    2  
    1  



### Multiple Variable For Loop Example  


```javascript
var numSheep = 4;
var monthsToPrint = 12;

for ( var monthNumber = 1; monthNumber <= monthsToPrint; monthNumber++){
  numSheep = numSheep * 4;
  console.log("There will be " + numSheep + " sheep after " + monthNumber + " month(s)!");
};
```

=> logs to console:  

    There will be 16 sheep after 1 month(s)!  
    There will be 64 sheep after 2 month(s)!  
    There will be 256 sheep after 3 month(s)!  
    There will be 1024 sheep after 4 month(s)!  



### <a name="loop-example"></a>Example While and For Loop Question

Question:

    The Hoover Dam has `19` generators of multiple types.  
    For simplicity, let’s say that the first `4` of these generators output  
    `62` megawatts, and the other `15` output `124` megawatts.  
    In `hooverDam.js`, the Dam Rangers have asked you to design a system  
    of two loops that turns each generator on in progression, and prints  
    the new total of megawatts generated.

    They’d like the first loop to be a while loop handling the first  
    `4` generators. Then, they’d like the second loop to be a for loop that  
    handles the other `15` generators. Each output line should resemble the
    following lines, with adjusted values for the `currentGen` and `totalMW`:

*output should look like:*  

    Generator #1 is on, adding 62 MW, for a total of 62 MW!  
    Generator #2 is on, adding 62 MW, for a total of 124 MW!  
    ...  
    Generator #5 is on, adding 124 MW, for a total of 372 MW!  

Answer:

```javascript
var currentGen = 1;
var totalGen = 6;
var totalMW = 0;

while (currentGen <= 4) {
  totalMW = totalMW + 62;
  console.log("Generator #" + currentGen +" is on, adding 62 MW, for a total of " + totalMW +" MW!");
currentGen++;

};

for (currentGen >= 5; currentGen <= totalGen; currentGen++){
  totalMW = totalMW + 124;
  console.log("Generator #" + currentGen +" is on, adding 124 MW, for a total of " + totalMW +" MW!");
};
```

*Using 2 loops we iterated through the first 4 generators with our `while loop`, then once it hit the 5th iteration it moved on to the `for loop.`*

=> prints to console

    Generator #1 is on, adding 62 MW, for a total of 62 MW!  
    Generator #2 is on, adding 62 MW, for a total of 124 MW!  
    Generator #3 is on, adding 62 MW, for a total of 186 MW!  
    Generator #4 is on, adding 62 MW, for a total of 248 MW!  
    Generator #5 is on, adding 124 MW, for a total of 372 MW!  
    Generator #6 is on, adding 124 MW, for a total of 496 MW!  

---


## Conditional Statements

### <a name="conditionals"></a>If Statements

*compares values or variables and executes code if the conditions of the expression are met*

```javascript
  var x = 5;
  if(x == 5){
    console.log("var x is equal to 5 so this appeared");
  } else {
    console.log("var x is not equal to 5 so this appeared");
  }
  var y = 17;
  if(y == 20){
    console.log("var y is equal to 20 so this appeared");
  } else {
    console.log("var y is not equal to 20 so this appeared");
  }
```

*here the variable x and y are being compared to integers. if the expression is true, the code will run, if not the `else` will run.*

=> prints to console

    var x is equal to 5 so this appeared  
    var y is not equal to 5 so this appeared


### <a name="switch"></a>Case/Switch Statements

The `switch` statement is used like an `else if` statement, however it is a bit easier to read and easier to write. If the expression/variable matches one of the cases, the code under its scope is ran. If there is no match, it returns the default code at the bottom.

#### Syntax

```javascript
var matchMe = "b";

switch (matchMe) {
  case "a":
	//	Run This Code
  console.log("variable matchMe = 'a', therefore, this was printed");
  break;
  case "b":
	//	Run This Code
  console.log("variable matchMe = 'b', therefore, this was printed");
  break;
  case "c":
	//	Run This Code
  console.log("variable matchMe = 'c', therefore, this was printed");
  break;    
  default:
  //  Run This Code
  console.log("variable matchMe matched NONE of the possible cases, therefore this was printed");
}


```

*since `matchMe = "b"` here => `"variable matchMe = 'b', therefore, this was printed"`*


#### Example

```javascript

var expr = "";

switch (expr) {
case "Oranges":
  console.log("Oranges are $0.59 a pound.");
  break;
case "Apples":
  console.log("Apples are $0.32 a pound.");
  break;
case "Bananas":
  console.log("Bananas are $0.48 a pound.");
  break;
case "Cherries":
  console.log("Cherries are $3.00 a pound.");
  break;
case "Mangoes":
case "Papayas":
  console.log("Mangoes and papayas are $2.79 a pound.");
  break;
default:
  console.log("Sorry, we are out of " + expr + ".");
}

// NOW CHANGE `expr` AND SEE OUR OUTPUTS

expr = "Papyas";      //  "Mangoes and papayas are $2.79 a pound."
expr = "Bananas";     //  "Bananas are $0.48 a pound."
expr = "Oranges";     //  "Oranges are $0.59 a pound."
expr = "Apples";      //  "Apples are $0.32 a pound."
expr = "Mangoes";     //  "Mangoes and papayas are $2.79 a pound."
expr = "Bananas";     //  "Bananas are $0.48 a pound."
expr = "bananas"      //  "Sorry, we are out of bananas."
expr = "Charmanders"  //  "Sorry, we are out of Charmanders."


```
With each change in var `expr`, we can see each output.  
- *If the `expr` variable matches one of the `case`'s in the `switch` statement, then it completes what is under it.*   
- *If var `expr` does not match any of the cases, it then returns whatever is under the `default` case.*  
  - _In the example above, notice how if `expr = "bananas"`, it returns `"Sorry, we are out of bananas."`, even though `Bananas` is one of the case statements. That is because it is **CASE SENSATIVE** if you are looking for a matching string value_
  - _If it returns a `default` case in this example, it will return whatever you set var `expr` to._
